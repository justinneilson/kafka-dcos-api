# Define a new line character to use in error strings.
define n


endef

# Define app version for use in docker build command for tagging containers
CONSUMER_VERSION := $(shell cat .VERSION |grep kafka-consumer-api |awk -F"=" '{print $$NF}')
PRODUCER_VERSION := $(shell cat .VERSION |grep kafka-producer |awk -F"=" '{print $$NF}')
EXPERIMENTAL := $(shell docker info --format '{{json .}}' |jq '.ExperimentalBuild')

ifeq ($(EXPERIMENTAL), true)
	DOCKER_BUILD_CMD := docker build --squash
else
	DOCKER_BUILD_CMD := docker build
endif

BUILD_EXPERIMENTAL := "docker build --squash"

# Build the consumer-api container image
.PHONY: build_consumer
build_consumer:
ifndef CONSUMER_VERSION
	$(error "$n'kafka-consumer-api' version number not defined.$nPlease add version number to .VERSION file using 'kafka-consumer-api=<version_number>'.$n$n)
else
	$(info "$nBuilding kafka-consumer-api container$n")
	$(DOCKER_BUILD_CMD) -t registry.gitlab.com/justinneilson/kafka-dcos-api/kafka-consumer-api -t registry.gitlab.com/justinneilson/kafka-dcos-api/kafka-consumer-api:$(CONSUMER_VERSION) kafka-consumer-api/
endif

.PHONY: push_consumer
push_consumer:
ifndef CONSUMER_VERSION
	$(error "$n$n'kafka-consumer-api' version number not defined.$nPlease add version number to .VERSION file using 'kafka-consumer-api=<version_number>'.$n$n)
else
	$(info "$n$n'Pushing 'kafka-consumer-api' to Gitlab'$n$n")
	docker push registry.gitlab.com/justinneilson/kafka-dcos-api/kafka-consumer-api
    docker push registry.gitlab.com/justinneilson/kafka-dcos-api/kafka-consumer-api:$(CONSUMER_VERSION)
endif

# Build the producer container image
.PHONY: build_producer
build_producer:
ifndef PRODUCER_VERSION
	$(error "$n$n'kafka-producer' version number not defined.$nPlease add version number to .VERSION file using 'kafka-producer=<version_number>'.$n$n)
else
	$(info "$n$nBuilding kafka-producer container$n$n")
	$(DOCKER_BUILD_CMD) -t registry.gitlab.com/justinneilson/kafka-dcos-api/kafka-producer -t registry.gitlab.com/justinneilson/kafka-dcos-api/kafka-producer:$(PRODUCER_VERSION) kafka-producer/
endif

.PHONY: push_producer
push_producer:
ifndef PRODUCER_VERSION
	$(error "$n$n'kafka-consumer-api' version number not defined.$nPlease add version number to .VERSION file using 'kafka-producer=<version_number>'.$n$n)
else
	$(info "$n$n'Pushing kafka-producer to Gitlab'$n$n")
	docker push registry.gitlab.com/justinneilson/kafka-dcos-api/kafka-producer
    docker push registry.gitlab.com/justinneilson/kafka-dcos-api/kafka-producer:$(CONSUMER_VERSION)
endif

# Build all container images
.PHONY: build_all
build_all:
ifndef CONSUMER_VERSION
	$(error "$n$nconsumer-api version number not defined.$nPlease add version number to .VERSION file using 'kafka-consumer-api=<version_number>'.$n$n)
else ifndef PRODUCER_VERSION
	$(error "$n$nproducer version number not defined.$n$nPlease add version number to .VERSION file using 'kafka-producer=<version_number>'.$n$n)
else
	make build_consumer
	make build_producer
endif

.PHONY: build_all_push
build_all_push:
ifndef CONSUMER_VERSION
	$(error "$n$nconsumer-api version number not defined.$nPlease add version number to .VERSION file using 'kafka-consumer-api=<version_number>'.$n$n)
else ifndef PRODUCER_VERSION
	$(error "$n$nproducer version number not defined.$n$nPlease add version number to .VERSION file using 'kafka-producer=<version_number>'.$n$n)
else
	make build_consumer
	make push_consumer
	make build_producer
	make push_producer
endif
