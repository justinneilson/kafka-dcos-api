package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
	"github.com/Shopify/sarama"
	"log"
	"encoding/json"
	"math/rand"
	"time"
	"os"
)

var (
	brokerList = kingpin.Flag("broker-list", "List of brokers to connect to").Default("localhost:9092").Strings()
	topic      = kingpin.Flag("topic", "Kafka topic name").Default("example").String()
)

type Quote struct {
	Author string `json:"author"`
	Text string `json:"text"`
}

func randomNumber(top int) int {
	return rand.Intn(top)
}

func main() {

	log.SetOutput(os.Stdout)

	var quoteList []Quote
	err := json.Unmarshal([]byte(quotes), &quoteList)
	if err != nil {
		log.Fatal(err)
	}

	kingpin.Parse()
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer(*brokerList, config)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()
	
	count := 0
	for i := 0; i < len(quoteList) - 1; i++ {
		count = count + 1
		kafkaMessage, err := json.Marshal(quoteList[randomNumber(len(quoteList) - 1 )])

		msg := &sarama.ProducerMessage{
			Topic: *topic,
			Value: sarama.StringEncoder(kafkaMessage),
		}
		partition, offset, err := producer.SendMessage(msg)
		if err != nil {
			panic(err)
		}
		log.Printf("message delivered : topic(%s)⇒partition(%d)⇒offset(%d)\n", *topic, partition, offset)
		time.Sleep(1 * time.Second)
	}
}