## DCOS training:

Two separate apps for testing out deployments during the DCOS training sessions.
* [Consumer-API](kafka-consumer-api) consumes messages from a Kafka topic and returns the message via an API in JSON.
* [Producer](kafka-producer) generates messages in a specific Kafka topic that will be consumed by the `consumer-api`

<small>_NOTE: Both apps were written using Go 1.10.2_</small>

A `Makefile` is provided to build the app containers:
```bash
make build_consumer
make build_producer
make build_all
```
Apps run on base image `alpine:3.7` and are built on `golang:1.10-alpine3.7`

_NB: Both apps require a running Kafka cluster!_

##### Run `kafka-consumer-api`:
```bash
docker run -d -p 8080:8080 --name kafka-consumer-api kafka-consumer-api:<version> ./kafka-consumer-api --broker-list=<kafka_broker_ip>:<broker_port> --topic=<kafka_topic>
```
API endpoint: `/api/messages`

##### Run `kafka-producer`:
```bash
docker run -d --name kafka-producer kafka-producer:<version> --broker-list=<broker_ip>:<broker_port> --topic=<kafka_topic>
```
