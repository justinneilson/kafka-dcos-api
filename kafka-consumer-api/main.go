package main

import (
	"gopkg.in/alecthomas/kingpin.v2"

	"github.com/Shopify/sarama"
	"net/http"
	"log"
	"os"
)

var (
	brokerList        = kingpin.Flag("broker-list", "List of brokers to connect").Default("localhost:9092").Strings()
	topic             = kingpin.Flag("topic", "Topic name").Default("default").String()
	partition         = kingpin.Flag("partition", "Partition number").Default("0").String()
	offsetType        = kingpin.Flag("offsetType", "Offset Type (OffsetNewest | OffsetOldest)").Default("-1").Int()
	messageCountStart = kingpin.Flag("messageCountStart", "Message counter start from:").Int()
)

func main() {

	log.SetOutput(os.Stdout)

	kingpin.Parse()
	config := sarama.NewConfig()
	config.Consumer.Return.Errors = true

	brokers := *brokerList
	master, err := sarama.NewConsumer(brokers, config)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := master.Close(); err != nil {
			panic(err)
		}
	}()

	consumer, err := master.ConsumePartition(*topic, 0, sarama.ReceiveTime)
	if err != nil {
		panic(err)
	}

	server := http.NewServeMux()
	setupApi(server, consumer)
	log.Println("kafka broker address: ", brokers)
	log.Println("starting server on :8080")
	err = http.ListenAndServe(":8080", server)
	panic(err)
}
